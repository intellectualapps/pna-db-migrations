# README #

This is the database migration repo for the PNA App. All changes to the database will be through migrations using the Flyway tool.

1. Create a clone of this repo.
2. Rename the "(rename this file to just gradle.properties)gradle.properties" file to "gradle.properties"
3. Edit the values in the gradle.properties file to represent your local environment (mysqlUser, mysqlPassword, dbUrl, migrationDir) 

To check the status of migrations on your system run:
gradle flywayinfo

To run the migrations against your local database run:
gradle flywaymigrate

If you do not have gradle installed then substitute "gradle" with "./gradlew" in the commands above.