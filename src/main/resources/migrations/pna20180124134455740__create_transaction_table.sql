CREATE TABLE IF NOT EXISTS transaction (
  id int(12) NOT NULL AUTO_INCREMENT,
  member_id int(5) NOT NULL,
  status varchar(10) NOT NULL,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;