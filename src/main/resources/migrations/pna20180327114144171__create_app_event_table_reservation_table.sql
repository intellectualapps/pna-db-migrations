CREATE TABLE app_event_table_reservation
(
    member_id int(5) NOT NULL,
    event_id varchar(100) NOT NULL,
    table_reservation_type enum('REGULAR', 'VIP') NOT NULL DEFAULT 'REGULAR',
    reference_code varchar(30) NOT NULL,
    created_at DATETIME NOT NULL,
    PRIMARY KEY (member_id, event_id),
    FOREIGN KEY (event_id) REFERENCES app_event(id),
    FOREIGN KEY (reference_code) REFERENCES `transaction`(reference_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;