CREATE TABLE app_event_rsvp
(
  member_id INT(5) NOT NULL,
  event_id VARCHAR(100) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (member_id, event_id),
  FOREIGN KEY (event_id) REFERENCES app_event(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
