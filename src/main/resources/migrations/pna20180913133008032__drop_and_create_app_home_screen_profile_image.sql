DROP TABLE `app_home_screen_profile_image`;
CREATE TABLE `app_home_screen_profile_image` (
  `image_url` varchar(255) NOT NULL,
  PRIMARY KEY (`image_url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;