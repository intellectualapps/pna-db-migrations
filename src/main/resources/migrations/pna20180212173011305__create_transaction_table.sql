DROP TABLE IF EXISTS `transaction`;

CREATE TABLE `transaction`
(
  reference_code varchar(30) NOT NULL,
  email varchar(100) NOT NULL,
  access_code varchar(200) DEFAULT NULL,
  amount varchar(20) DEFAULT NULL,
  response_code char(3) DEFAULT NULL,
  response_message varchar(300) DEFAULT NULL,
  transaction_date TIMESTAMP,
  PRIMARY KEY (reference_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
