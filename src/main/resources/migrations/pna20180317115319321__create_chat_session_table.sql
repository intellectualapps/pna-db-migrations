CREATE TABLE chat_session
(
  id varchar(100) NOT NULL,
  member_id int(5) NOT NULL,
  other_member_id int(5) NOT NULL,
  created_at DATETIME NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT Chat_Members UNIQUE (member_id, other_member_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;