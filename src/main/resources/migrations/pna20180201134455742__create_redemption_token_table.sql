CREATE TABLE `redemption_token` (
  `purpose` varchar(64) NOT NULL,
  `token` varchar(100) NOT NULL,
  PRIMARY KEY (`purpose`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8