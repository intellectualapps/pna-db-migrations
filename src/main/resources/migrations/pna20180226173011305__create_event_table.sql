CREATE TABLE `app_event`
(
  `id` varchar(100) NOT NULL,
  `title` varchar(50) NOT NULL,
  `state_id` int(11) NOT NULL,
  `location` varchar(100) NOT NULL,
  `street` varchar(50) NOT NULL,
  `street_number` int(10) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `venue` varchar(100) NOT NULL,
  `posted_by` varchar(100) NOT NULL,
  `dress_code` text DEFAULT NULL,
  `contact` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`state_id`) REFERENCES state(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;