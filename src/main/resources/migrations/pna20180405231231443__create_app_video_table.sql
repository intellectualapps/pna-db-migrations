CREATE TABLE app_video_home_screen
(
    title VARCHAR(255) NOT NULL,
    video_url VARCHAR(255) NOT NULL,
    PRIMARY KEY (title)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;