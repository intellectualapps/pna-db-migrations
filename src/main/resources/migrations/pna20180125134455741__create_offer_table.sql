CREATE TABLE IF NOT EXISTS offer (
  id varchar(24) NOT NULL,
  title varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  business_name varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  address varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  type enum('ONE_OFF', 'MULTIPLE') NOT NULL DEFAULT 'ONE_OFF',
  photo varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  details text NOT NULL,
  status enum('ACTIVE', 'INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  offer_level enum('REGULAR','PREMIUM') NOT NULL DEFAULT 'REGULAR',
  created_at date NOT NULL,
  starts_at date NOT NULL,
  stops_at date NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;