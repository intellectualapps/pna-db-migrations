CREATE TABLE `password_reset_token` (
  `id` VARCHAR(100) NOT NULL,
  `token` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

