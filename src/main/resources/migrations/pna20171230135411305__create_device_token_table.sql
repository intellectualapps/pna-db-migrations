CREATE TABLE device_token
(
  email varchar(100) NOT NULL,
  token varchar(200) DEFAULT NULL,
  platform varchar(45) DEFAULT NULL,
  uuid varchar(300) DEFAULT NULL,
  PRIMARY KEY (email)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;