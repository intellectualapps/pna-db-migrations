ALTER TABLE app_event
ADD is_paid_event tinyint(1) NOT NULL DEFAULT 0;

ALTER TABLE app_event
ADD has_table_reservation tinyint(1) NOT NULL DEFAULT 0;