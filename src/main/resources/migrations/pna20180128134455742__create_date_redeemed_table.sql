CREATE TABLE IF NOT EXISTS date_redeemed (
  member_id int(5) NOT NULL,
  offer_id varchar(24) NOT NULL,
  created_at date NOT NULL,
  PRIMARY KEY (member_id, offer_id, created_at),
  FOREIGN KEY (member_id) REFERENCES redeemed_offer(member_id),
  FOREIGN KEY (offer_id) REFERENCES redeemed_offer(offer_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;