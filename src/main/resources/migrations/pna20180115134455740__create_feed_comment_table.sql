CREATE TABLE IF NOT EXISTS feed_comment (
  id int(12) NOT NULL AUTO_INCREMENT,
  feed_id int(5) NOT NULL,
  comment text,
  user_id int(5) NOT NULL,
  comment_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;