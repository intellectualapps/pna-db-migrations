CREATE TABLE app_pnadaily_home_screen
(
    title VARCHAR(255) NOT NULL,
    image_url VARCHAR(255) NOT NULL,
    content_url VARCHAR(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO app_pnadaily_home_screen (title, image_url, content_url) VALUES
("PNA Daily", "http://nothing.com/", "https://api.bulamausuf.com/api/v1/pna-daily"),
("PNA Daily", "http://nothing.com/", "https://api.bulamausuf.com/api/v1/pna-daily"),
("PNA Daily", "http://nothing.com/", "https://api.bulamausuf.com/api/v1/pna-daily"),
("PNA Daily", "http://nothing.com/", "https://api.bulamausuf.com/api/v1/pna-daily"),
("PNA Daily", "http://nothing.com/", "https://api.bulamausuf.com/api/v1/pna-daily"),
("PNA Daily", "http://nothing.com/", "https://api.bulamausuf.com/api/v1/pna-daily"),
("PNA Daily", "http://nothing.com/", "https://api.bulamausuf.com/api/v1/pna-daily"),
("PNA Daily", "http://nothing.com/", "https://api.bulamausuf.com/api/v1/pna-daily"),
("PNA Daily", "http://nothing.com/", "https://api.bulamausuf.com/api/v1/pna-daily"),
("PNA Daily", "http://nothing.com/", "https://api.bulamausuf.com/api/v1/pna-daily");