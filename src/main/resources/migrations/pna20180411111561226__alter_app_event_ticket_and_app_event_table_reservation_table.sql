ALTER TABLE app_event_ticket
ADD quantity INT(11) NOT NULL DEFAULT 0;

ALTER TABLE app_event_table_reservation
ADD quantity INT(11) NOT NULL DEFAULT 0;