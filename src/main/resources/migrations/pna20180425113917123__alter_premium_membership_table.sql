ALTER TABLE premium_member ADD authorization_token VARCHAR(20) AFTER access_token;

ALTER TABLE premium_member ADD expiry_date DATETIME AFTER updated_at;