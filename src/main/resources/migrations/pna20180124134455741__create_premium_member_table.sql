CREATE TABLE IF NOT EXISTS premium_member (
  member_id int(5) NOT NULL,
  access_token varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (member_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;