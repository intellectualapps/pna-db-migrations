CREATE TABLE IF NOT EXISTS redeemed_offer (
  member_id int(5) NOT NULL,
  offer_id varchar(24) NOT NULL,
  count int(10) NOT NULL,
  PRIMARY KEY (member_id, offer_id),
  FOREIGN KEY (offer_id) REFERENCES offer(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;