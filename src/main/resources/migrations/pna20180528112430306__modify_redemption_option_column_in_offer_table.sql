ALTER TABLE offer MODIFY COLUMN redemption_option ENUM('QR', 'CARD', 'ANY');

UPDATE offer SET redemption_option='ANY' WHERE redemption_option='';