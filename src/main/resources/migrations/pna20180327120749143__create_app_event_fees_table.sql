CREATE TABLE app_event_fee
(
    event_id varchar(100) NOT NULL,
    regular_ticket char(11) NULL,
    vip_ticket char(11) null,
    regular_table_reservation char(11) NULL,
    vip_table_reservation char(11) NULL,
    PRIMARY KEY (event_id),
    FOREIGN KEY (event_id) REFERENCES app_event(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;