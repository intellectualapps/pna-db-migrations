DROP TABLE `app_pnadaily_home_screen`;

CREATE TABLE `app_pnadaily_home_screen` (
  `title` varchar(255) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `content_url` varchar(255) NOT NULL,
  PRIMARY KEY (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;